# Importando paquetes de panda
import pandas as pd

# definiendo datos en un diccionario
data = {
        'Ingenierias': ['Ambiental', 'Sistemas', 'Electronica', 'la otra que no me acuerdo'],
        'Estudiantes': [1000, 2000, 700, 400]
    }

# Convert the dictionary into DataFrame
df = pd.DataFrame(data)

# select two columns
print(df)
